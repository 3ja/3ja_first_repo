import win
import pyglet
from pyglet.gl import *
import time
class main(win.Win):
    def __init__(self,*args, **kwargs):
        #win.Win.__init__(self,width=320,height=240,*args, **kwargs)
        win.Win.__init__(self,*args, **kwargs)
        self.setupGL()
    def setupGL(self):
        print("setting up gl")
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45,self.width,self.height,1,1000)
    def update(self,dt):
        pass
    def on_draw(self):
        pass
def run():
    #w = roulette(fullscreen = True)
    w = main()
    pyglet.app.run()
if __name__ == "__main__":
    run()
