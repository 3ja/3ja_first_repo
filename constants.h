#ifndef CONSTANTS_H
#define CONSTANTS_H

//------------
//input constants
//-----------
#define ESCAPE 27
#define PAGE_UP 73
#define PAGE_DOWN 81
#define UP_ARROW 72
#define DOWN_ARROW 80
#define LEFT_ARROW 75
#define RIGHT_ARROW 77

//screen constants
GLuint SCREEN_WIDTH = 640;
GLuint SCREEN_HEIGHT = 480;
const GLfloat SCREEN_CLEAR_COLOR[4] = {0.0f,0.0f,0.0f,0.0f};

//cube vectors
static const GLint CUBEVERTICES[6][4][3] = {
	0,1,0,  1,1,0,  1,1,1,  0,1,1,
	0,0,0,  0,1,0,  0,1,1,  0,0,1,
	1,0,0,  0,0,0,  0,0,1,  1,0,1,
	1,1,0,  1,0,0,  1,0,1,  1,1,1,
	0,1,1,  1,1,1,  1,0,1,  0,0,1,
	0,0,0,  1,0,0,  1,1,0,  0,1,0
};

static const GLfloat CUBENORMALS[6][4][3] = {
	0,1,0,	0,1,0,	0,1,0,	0,1,0,
	-1,0,0,	-1,0,0,	-1,0,0,	-1,0,0,
	0,-1,0,	0,-1,0,	0,-1,0,	0,-1,0,
	1,0,0,	1,0,0,	1,0,0,	1,0,0,
	0,0,1,	0,0,1,	0,0,1,	0,0,1,
	0,0,-1,	0,0,-1,	0,0,-1,	0,0,-1
};

static const GLfloat CUBETEXTUREUV[4][2] = {
    0,0, 0,1, 1,1, 1,0
};

/* normals reversed
static const GLint CUBENORMALS[] = {
	0,-1,0,	0,-1,0,	0,-1,0,	0,-1,0,
	1,0,0,	1,0,0,	1,0,0,	1,0,0,
	0,1,0,	0,1,0,	0,1,0,	0,1,0,
	-1,0,0,	-1,0,0,	-1,0,0,	-1,0,0,
	0,0,-1,	0,0,-1,	0,0,-1,	0,0,-1,
	0,0,1,	0,0,1,	0,0,1,	0,0,1
};
*/
static const GLfloat CUBEFACECOLORS[] = {
    1,0,0,    1,0,0,    1,0,0,    1,0,0,    //RED
    0,1,0,    0,1,0,    0,1,0,    0,1,0,    //GREEN
    0,0,1,    0,0,1,    0,0,1,    0,0,1,    //BLUE
    1,1,0,    1,1,0,    1,1,0,    1,1,0,    //YELLOW
    1,0,1,    1,0,1,    1,0,1,    1,0,1,    //MAGENTA
    0,1,1,    0,1,1,    0,1,1,    0,1,1     //CYAN
};

static const GLubyte ALLINDICES[] = {
	0,1,2,3,
	4,5,6,7,
	8,9,10,11,
	12,13,14,15,
	16,17,18,19,
	20,21,22,23};

static const GLubyte SIDEINDICES[6][4] = { 
	{4,5,6,7}, 
	{0,4,7,3}, 
	{1,0,3,2}, 
	{5,1,2,6}, 
	{7,6,3,2},
	{0,1,5,4}};

#endif
