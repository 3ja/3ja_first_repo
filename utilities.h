#ifndef UTILITIES_H
#define UTILITIES_H
#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdlib.h>
GLfloat * add3fv(GLfloat * u, GLfloat * v)
{
    GLfloat * r = (GLfloat *) malloc( sizeof(GLfloat)*3 );
    r[0] = u[0] + v[0];
    r[1] = u[1] + v[1];
    r[2] = u[2] + v[2];
    return r;
}
GLfloat * mult3fv(GLfloat * u, GLfloat c)
{
    GLfloat * r = (GLfloat *) malloc( sizeof(GLfloat)*3 );
    r[0] = u[0]*c;
    r[1] = u[1]*c;
    r[2] = u[2]*c;
    return r;
}
#endif
