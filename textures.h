#ifndef TEXTURES_H
#define TEXTURES_H

#include <assert.h>

#define numberTextures 1
#define textureDim 16
GLubyte textures[numberTextures][textureDim][textureDim][4];
GLubyte * getTexture(GLuint texturePosition)
{
    assert(texturePosition < numberTextures && texturePosition >= 0);
    return &***textures[texturePosition];
}
void makeTexture(GLuint texturePosition, GLvoid (*imageFunc)(int,int,GLubyte *))
{
    int i,j;
    for(i = 0; i < textureDim; i++)
    {
        for(j = 0; j < textureDim; j++)
        {
            imageFunc(i,j,textures[texturePosition][i][j]);
        }
    }
}
GLvoid setImage1(int i, int j,GLubyte * data)
{
    if( i == j || i == -j)
    {
        //data = {255,0,255,255};
        data[0] = 255;
        data[1] = 0;
        data[2] = 255;
        data[3] = 255;
    }
    else
    {
        //data = {0,255,255,255};
        data[0] = 0;
        data[1] = 255;
        data[2] = 255;
        data[3] = 255;
    }
}
void makeAllTextures()
{
    makeTexture(0,&setImage1);
}

#endif
