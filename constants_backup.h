#ifndef CONSTANTS_H
#define CONSTANTS_H

//------------
//input constants
//-----------
#define ESCAPE 27
#define PAGE_UP 73
#define PAGE_DOWN 81
#define UP_ARROW 72
#define DOWN_ARROW 80
#define LEFT_ARROW 75
#define RIGHT_ARROW 77

//screen constants
GLuint SCREEN_WIDTH = 640;
GLuint SCREEN_HEIGHT = 480;
const GLfloat SCREEN_CLEAR_COLOR[4] = {0.0f,0.0f,0.0f,0.0f};

//cube vectors
static const GLint CUBEVERTICES[] = { 
                            0,0,0,
                            1,0,0,
                            1,0,1,
                            0,0,1,
                            0,1,0,
                            1,1,0,
                            1,1,1,
                            0,1,1 };
static const GLint CUBENORMALS[] = {0};

static const GLubyte SIDEINDICES[6][4] = { 
                                {4,5,6,7}, 
                                {0,4,7,3}, 
                                {1,0,3,2}, 
                                {5,1,2,6}, 
                                {7,6,3,2},
                                {0,1,5,4}};
static const GLubyte ALLINDICES[] = {4,5,6,7,0,4,7,3,1,0,3,2,5,1,2,6,7,6,3,2,0,1,5,4};
//static const GLubyte ALLINDICES[] = {12,15,18,21,0,12,21,9,3,0,9,6,15,3,6,18,21,18,9,6,0,3,15,12};
#endif
