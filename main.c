//---------
//system library includes
//---------

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <unistd.h>     // Header file for sleeping.
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.

//---------
//my includes
//---------
#include "constants.h"
#include "fake_vector.h"
#include "utilities.h"
#include "textures.h"

//---------
//system vars
//  these vars pertain to setting up the window for opengl
//--------

int windowId;

//---------
//other nasty global vars that I should try and get rid of eventually
//---------

GLuint firstFacetId;
GLuint textureId[numberTextures];


void keyPressed(unsigned char key, int x, int y)
{
    switch(key)
    {
        //if you try really hard, the following almost looks like python. Ironically, python does not have a switch statement.
        case ESCAPE:
            glutDestroyWindow(windowId);
            break;
        //arrow keys cases
        case UP_ARROW:
            break;
        case DOWN_ARROW:
            break;
        case LEFT_ARROW:
            break;
        case RIGHT_ARROW:
            break;
        default:
            break;
    }
}


//side convention 1 2 3 4 5 6 correlate to front left back right top bottom sides
//this function draws a face of a cube with given color in positive x y z quadrant
GLvoid drawCubeFace(GLuint side)
{
    int i = 0;
    glEnableClientState(GL_VERTEX_ARRAY);
    //the stuff here in comments actually works
    //for(i = 0; i < 4; i++)
        //glVertex3iv(&CUBEVERTICES[SIDEINDICES[side][i]*3]);
    glVertexPointer(3,GL_INT,0,CUBEVERTICES);
    glDrawElements(GL_QUADS,4,GL_UNSIGNED_BYTE,SIDEINDICES[side]);
    glDisableClientState(GL_VERTEX_ARRAY);
}

GLvoid initCubeFaceDisplayLists()
{
    firstFacetId = glGenLists(6);
    int face = 0;
    for(face = 0; face < 6; face++)
    {
        glNewList(firstFacetId+face, GL_COMPILE);
        glBegin(GL_QUADS);
        int vert;
        for(vert = 0; vert < 4; vert++)
        {
            glTexCoord2fv(CUBETEXTUREUV[vert]);
            glNormal3fv(CUBENORMALS[face][vert]);
            glVertex3iv(CUBEVERTICES[face][vert]);
        }
        glEnd();
        glEndList();
    }
}
GLvoid initDisplayLists()
{
    firstFacetId = glGenLists(6);
    glNewList(firstFacetId, GL_COMPILE);
    glBegin(GL_QUADS);
    glTexCoord2f(0,1);
    glNormal3f(0,1,0);
    glVertex3f(-1,1,1);
    glTexCoord2f(1,1);
    glNormal3f(0,1,0);
    glVertex3f(1,1,1);
    glTexCoord2f(0,1);
    glNormal3f(0,1,0);
    glVertex3f(1,1,-1);
    glTexCoord2f(0,0);
    glNormal3f(0,1,0);
    glVertex3f(-1,1,-1);
    glEnd();
    glEndList();
}
GLvoid drawCubeImmediateMode()
{
    glBegin(GL_QUADS);
    glTexCoord2f(0,1);
    glNormal3f(0,1,0);
    glVertex3f(-1,1,1);
    glTexCoord2f(1,1);
    glNormal3f(0,1,0);
    glVertex3f(1,1,1);
    glTexCoord2f(0,1);
    glNormal3f(0,1,0);
    glVertex3f(1,1,-1);
    glTexCoord2f(0,0);
    glNormal3f(0,1,0);
    glVertex3f(-1,1,-1);
    glEnd();
}
GLvoid drawCubeWithFaces()
{
    int i = 0;
    for(i = 0; i < 6; i++){
        drawCubeFace(i);
    }
}
GLvoid drawCube()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glNormalPointer(GL_INT,0,CUBENORMALS);
    glVertexPointer(3,GL_INT,0,CUBEVERTICES);
    glColorPointer(3,GL_FLOAT,0,CUBEFACECOLORS);
    glDrawElements(GL_QUADS,24,GL_UNSIGNED_BYTE,ALLINDICES);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
} 
GLvoid drawSquare()
{
    GLfloat verts[] = {0,0,0, 1,0,0, 1,1,0, 0,1,0};
    GLfloat colors[] = {1,0,0, 0,1,0, 0,0,1, 1,1,1};
    GLubyte ind[] = {0,1,2,3};
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(3,GL_FLOAT,0,colors);
    glVertexPointer(3,GL_FLOAT,0,verts);
    glDrawElements(GL_QUADS,24,GL_UNSIGNED_BYTE,ind);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

}
GLvoid setCamera()
{
    printf("setting projection matrix\n");
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0,16/9,0.01,10000);
    glMatrixMode(GL_MODELVIEW);
}

GLvoid initTexture(int texIndex)
{
}

GLvoid setViewing(GLfloat * p, GLfloat * t, GLfloat * u)
{
    GLfloat distance = 1;
    GLfloat height = 1; // note atan(height/distance) gives viewing angle
    GLfloat * eye = add3fv(add3fv(p,mult3fv(t,distance)),mult3fv(u,height));
    gluLookAt(eye[0],eye[1],eye[2],p[0],p[1],p[2],u[0],u[1],u[2]);
}

GLvoid handleErrors()
{
    GLenum err = glGetError();
    if(err != GL_NO_ERROR)
    {
        printf("Got glError %s", gluErrorString(err));
    }
}
GLvoid drawScene()
{
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glRotatef(1,1,1,1);
    //glColor3f(1,0,0);
    //drawCube();
    glEnable(GL_TEXTURE_2D);
    //glDisable(GL_LIGHTING);
    glBindTexture(GL_TEXTURE_2D,textureId[0]);
    int i;
    for(i = 0; i < 6; i++)
        glCallList(firstFacetId+i);
    GLfloat p[] = {0.0f,0.0f,0.0f};
    GLfloat t[] = {0.0f,1.0f,0.0f};
    GLfloat u[] = {0.0f,0.0f,1.0f};
    //setViewing(p,t,u);
    glutSwapBuffers();

    handleErrors();
}

GLvoid onResize(GLsizei width, GLsizei height)
{
    if(height == 0)
        height = 1;

    //viewport transform??
        
    //set up the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.0f,100.0f);
    glLoadIdentity();
    glOrtho(-1.5,1.5,-1.5*(GLfloat)width/(GLfloat)height,1.5*(GLfloat)width/(GLfloat)height,-10.0,10.0);
    glMatrixMode(GL_MODELVIEW);
}

GLvoid initLights()
{
    GLfloat lightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat lightPosition[] = { 0.0f, 2.0f, 2.0f, 1.0f };

    glLightfv(GL_LIGHT1,GL_DIFFUSE,lightDiffuse);
    glLightfv(GL_LIGHT1,GL_POSITION,lightPosition);
    glLightfv(GL_LIGHT1,GL_AMBIENT,lightAmbient);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);
}

GLvoid idleFunc()
{
    glutPostRedisplay();
}
GLvoid initTextures()
{
    //texture stuff
    makeAllTextures();
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    //bind all the textures
    //TODO move this stuff to textures.h
    glGenTextures(numberTextures,textureId);
    int texIndex = textureDim;
    for(texIndex = 0; texIndex < numberTextures; texIndex++)
    {
        glBindTexture(GL_TEXTURE_2D,textureId[texIndex]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureDim, textureDim, 0, GL_RGBA, GL_UNSIGNED_BYTE, getTexture(texIndex));
    }
}


GLvoid initGl(GLsizei width, GLsizei height)
{
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    glClearDepth(1.0);
    onResize(width,height);
    initLights();
    initTextures();
    initCubeFaceDisplayLists();
    //initDisplayLists();
}

int main(int argc, char **argv) 
{  

    //---------
    //initialize glut
    //---------
    printf("initializing glut\n");
    /* Initialize GLUT state - glut will take any command line arguments that pertain to it or 
    *        X Windows - look at its documentation at http://reality.sgi.com/mjk/spec3/spec3.html */  
    glutInit(&argc, argv);  

    /* Select type of Display mode:   
    *      Double buffer 
    *           RGBA color
    *                Alpha components supported 
    *                     Depth buffer */  
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);  

    /* get a 640 x 480 window */
    glutInitWindowSize(640, 480);  

    /* the window starts at the upper left corner of the screen */
    glutInitWindowPosition(0, 0);  

    /* Open a window */  
    windowId = glutCreateWindow("3ja");
                    

    //---------
    //Setup callbacks
    //---------
    glutKeyboardFunc(&keyPressed);
    glutReshapeFunc(&onResize);
    glutDisplayFunc(&drawScene);  
    glutIdleFunc(&idleFunc);
    
    //---------
    //initialize the window
    //---------
    initGl(SCREEN_WIDTH,SCREEN_HEIGHT);

    //---------
    //Begin mainloop, this will never return :(
    //---------
    glutMainLoop();
    return 1;
}
