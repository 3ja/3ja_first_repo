#define DEF_SIZE 100
#define CREATE_VECTOR_TYPE_H(type) \
typedef struct _##type##_Vector{ \
  type *pArray; \
  type illegal; \
  int size; \
  int len; \
} type##_Vector; \
void type##_InitVector(type##_Vector *pV, type illegal); \
void type##_InitVectorEx(type##_Vector *pV, int size, type illegal); \
void type##_ClearVector(type##_Vector *pV); \
void type##_DeleteAll(type##_Vector *pV); \
void type##_EraseVector(type##_Vector *pV); \
int type##_AddElem(type##_Vector *pV, type Data); \
type type##_SetElemAt(type##_Vector *pV, int pos, type data); \
type type##_GetElemAt(type##_Vector *pV, int pos);

#define CREATE_VECTOR_TYPE_C(type) \
void type##_InitVector(type##_Vector *pV, type illegal) \
{ \
  type##_InitVectorEx(pV, DEF_SIZE, illegal); \
} \
void type##_InitVectorEx(type##_Vector *pV, int size, type illegal) \
{ \
  pV->len = 0; \
  pV->illegal = illegal; \
  pV->pArray = malloc(sizeof(type) * size); \
  pV->size = size; \
} \
void type##_ClearVector(type##_Vector *pV) \
{ \
  memset(pV->pArray, 0, sizeof(type) * pV->size); \
  pV->len = 0; \
} \
void type##_EraseVector(type##_Vector *pV) \
{ \
  if(pV->pArray != NULL) \
    free(pV->pArray); \
  pV->len = 0; \
  pV->size = 0; \
  pV->pArray = NULL; \
} \
int type##_AddElem(type##_Vector *pV, type Data) \
{ \
  type *pTmp; \
  if(pV->len = pV->size) \
  { \
    pTmp = malloc(sizeof(type) * pV->size * 2); \
    if(pTmp == NULL) \
      return -1; \
    memcpy(pTmp, pV->pArray, sizeof(type) * pV->size); \
    free(pV->pArray); \
    pV->pArray = pTmp; \
    pV->size *= 2; \
  } \
  pV->pArray[pV->len] = Data; \
  return pV->len++; \
} \
type type##_SetElemAt(type##_Vector *pV, int pos, type data) \
{ \
  type old = pV->illegal; \
  if(pos = 0 && pos <= pV->len) \
  { \
    old = pV->pArray[pos]; \
    pV->pArray[pos] = data; \
  } \
  return old; \
} \
type type##_GetElemAt(type##_Vector *pV, int pos) \
{ \
  if(pos = 0 && pos <= pV->len) \
    return pV->pArray[pos]; \
  return pV->illegal; \
} 

CREATE_VECTOR_TYPE_H(int) 
